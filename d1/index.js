/*
Aggregation in MongoDB and Query Case Studies.

	An aggregate is a cluster of things that have come or been brought together.

	in mongodb, aggregation operations group values from multiple documents together.

	Aggregations are needed when your application needs a form of information that is not visibly available from your documents.



Use Cases for Aggregaions
	
		1. you would like to know which students have not finished their courses yet.

		2. Count the total number of courses a student has completed.

		3.Determine the number of students who completed a specified course.

	if you find yourself having these types of use cases, aggregations can come in handy.


Agg Pipelines.

	the agg pipe

	esch stage pass thru a pipeline

	Pipeline stages do not need to produce one output document for every input document. Eg. some stages may generate new documents or filter out documents.



Agg Pipelines Syntax.
	A pipeline can have one or more stages.

*/


/*
Code Along

Aggregation
	- Definition

	Aggregate Methods
	1. Match
	2. Group
	3. Sort
	4. Project

	Aggregation pipelines

	Operations
	1. Sum
	2. Max
	3. Min
	4. Avg


MongoDB Aggregation
	- Used to generate manipulated data and perform operations to create filtered results that helps in analyzing data.

	- Compared to doung CRUD Operations on our data from our previous sessions, aggregation gives us access to manipulate, filter, and compute for results, providing us with information to make necessary development decisions.

	- Aggregations in MongoDB are very flexible and you can form your own aggregation pipeline depending on the need of your application.


AGGREGATE METHODS

MATCH Method: $match
	- THe "$match" is used to pass the documents that meet the specified conditions to the next pipeline stage or aggregation process.
	Syntax:
		{ $match: (field: value) }
*/
db.fruits.aggregate([
	{$match: {onSale: true}}
]);

db.fruits.aggregate([
	{$match: {stock: {$gte: 20}}}
]);


/*
GROUP: $group
	- The $group is used to group elements together field-value pairs using the data from the grouped elements.
	Syntax:
		{$group: {_id: "value", fieldResult: {"valueResult"}}}
*/
db.fruits.aggregate([
	{$group: {_id: "$supplier_id", total: {$sum: "$stock"}}}
]); //dito ginurupo nga lahat ng mga parehong supplier_id tapos sinum lahat ng stocks ng mga parehong supplier_id.


/*
SORT: $sort
	- The $sort can be used when changing the order of aggregated results.
	- Providing a value of -1 will sort the documents in a descending order.
	- Providing a value of 1 will sort the documents in an ascending order.
	Syntax:
		{$sort {field: 1/-1}}
*/
db.fruits.aggregate([
	{$match: { onSale: true}},
	{$group: { _id: "$supplier_id", total: {$sum: "$stock"}}},
	{$sort: {total: -1}} //dito na sort lang yung mga value, decending or ascending
]);


/*
PROJECT: $project
	- The $project can be used when aggregating data to include or exclude fields from the returned results.
	Syntax:
		{$project: {field: 1/0}}
*/
db.fruits.aggregate([
	{$match: { onSale: true}},
	{$group: { _id: "$supplier_id", total: {$sum: "$stock"}}},
	{$project: {_id: 0}}
]);

db.fruits.aggregate([
	{$match: {onSale: true}},
	{$group: {_id: "$supplier_id", total: {$sum: "$stock"}}},
	{$project: {_id: 0}},
	{$sort: {total: -1}}
]);


/*
AGGREGATION PIPELINE
	- The aggregation pipeline in Mongodb is a framework for data aggregation
	- Each stage transforms the documents as they pass through the dedlines.
	Syntax:
		db.collectionName.aggregate([
			{stageA},
			{stageB},
			{stageC}
		])
*/





//OPERATORS
/*
	1. $sum - gets the total of everything.
*/
db.fruits.aggregate([
	{$group: {_id: "$supplier_id", total: {$sum: "$stock"}}}
]);

/*
	2. $max - gets the highest value out of everything else.
*/
db.fruits.aggregate([
	{$group: {_id: "$supplier_id", max_price: {$max: "$price"}}}
]); //kinukuha nya ang pinaka mataas na presyo

/*
	3. $min - gets the lowest value out of everything else
*/
db.fruits.aggregate([
	{$group: {_id: "$supplier_id", min_price: {$min: "$price"}}}
]);

/*
	4. $avg - gets the average value of all the fields
*/
db.fruits.aggregate([
	{$group: {_id: "$supplier_id", avg_price: {$avg: "$price"}}}
]);